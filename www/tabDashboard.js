/*
	Drop Drop Project
	iCOMMS Group - University of Cape Town
	Initial Author: Patrick Rein (pre), patrick.rein@gmail.com
 */

var WNTabDashboard = function() {
	
	var refreshMeterReadingBox = function() {
		$.when(WNDatabase.getTimeOfLatestReading())
		.done(function(timestampLatestReading) {
			if(!timestampLatestReading) {
				$("#meter-reading-box").show();
				return;
			}
			var ageOfLatestReadingInHours = moment.duration(moment() - timestampLatestReading).asHours();
			if(ageOfLatestReadingInHours > 23) {
				$("#meter-reading-box").show();
			} else {
				$("#meter-reading-box").hide();
			}
		});
	}
	
	var setUpStandardMeterReadingBox = function() {
		setUpMeterReadingSaving("#lower-meter-reading-box");
	}

	var setUpMeterReadingSaving = function(id, afterSavingCallback) {
		$(id + " #save-reading-button").click(function(event) {
			event.stopPropagation();
			var formField = $(id + " #save-reading-button").parent().parent().children("div.columns:first-child").children("input");
			var reading = formField.val();
			$(id + " > div > div > small.error").remove();
			var error = false;
			
			if((!$.isNumeric(reading)) || (parseInt(reading, 10) < 0)) {
				error = "You can only enter positive numeric data!";
			};
			
			$.when(WNDatabase.getReadingOfLatestReading())
			.done(function(lastReading) {
				if(!error) {
					if($.isNumeric(reading)) {
						var readingNumber = parseInt(reading, 10);
						if(lastReading) {
							if(readingNumber >= lastReading) {
								// Store it as it is a valid reading.
								WNDatabase.storeMeterReading(moment(), readingNumber);
							} else {
								error = "You maybe made an error entering the data, as the reading is smaller than the last one you entered.";
							}
						} else {
							// Store it as it is the first reading.
							WNDatabase.storeMeterReading(moment(), readingNumber);
						}
					}
				}
				if(error) {
					// Display Error
					$(id + " > div > div.columns:first-child").append("<small class='error'>" + error + "</small>");
				} else {
					// Clear form and hide notification
					formField.val("");
					if(afterSavingCallback) {
						afterSavingCallback();
					}
					_refreshStatusTab();
				}
			})
			
			return false;
		});
	}

	var setUpMeterReadingBox = function() {
		$("#meter-reading-box").hide();
		
		$("#meter-reading-box-action1").click(function(event) {
			$("#meter-reading-box").hide();
			
			// And clear errors and form field
			var formField = $("#meter-reading-box > div > div.columns:first-child > input");
			formField.val("");
			
			$("#meter-reading-box > div > div > small.error").remove();
		});
		
		setUpMeterReadingSaving("#meter-reading-box", function() { $("#meter-reading-box").hide() });
	}
	
	var refreshReadingsList = function() {
		Handlebars.registerHelper('human-date', function() {
			  return new Handlebars.SafeString(
			    this.timestamp.format("Do MMM YY")
			  );
		});
		
		$.when(WNDatabase.getReadings(30))
		.done(function(readings) {
			
			var source   = $("#reading-list-template").html();
			var template = Handlebars.compile(source);
			var html = template({"readings" : readings});
		
			$("#readings-list").children().remove();
			$("#readings-list").append(html);
		
			$(".delete-readings").click(function(event) {
				event.stopPropagation();
				
				var timestamp = $(this).attr("data-timestamp");
				StarTrack.track("rmReadReq", timestamp);
				
				var confirmCallback = function(button) {
					if(button == 2) {
						// Yes pressed
						WNDatabase.deleteReading(timestamp);
						_refreshStatusTab()
					}
				}
				if(navigator.notification) {
					navigator.notification.confirm("Do you really want to delete the reading from " + moment(parseInt(timestamp,10)).format("Do MMM YY") + "?", confirmCallback, "Delete Reading", "No,Yes");
				} else {
					WNDatabase.deleteReading(timestamp);
					_refreshStatusTab()
				}
				
			});
			
		});
		
	}

	var refreshUsagesList = function() {
		$.when(WNDatabase.getWeeklyReadingsCount())
		.done(function(weeks) {
			$("#usages-list").children().remove();
			var source   = $("#usage-list-item-template").html();
			var template = Handlebars.compile(source);
			
			$.each(weeks, function(index, week) {
				
				var weekTimestamp = moment().isoWeek(week.week).year(week.year)
				var weekStart= moment(weekTimestamp).startOf("week");
				var weekEnd = moment(weekTimestamp).endOf("week");
				
				if(week.readings > 2) {
					$.when(WNDatabase.getDailyUsageFromTo(weekStart, weekEnd))
					.done(function(usages) {
						// Render template
						var html = template({"usages" : usages, "week" : index});
						
						// Add to node
						$("#usages-list").append(html);
						
						// Set up chart
						$("#usages-list > #chart-week-" + index);
						
					});
				}
				
			});
		});
	}

	var refreshCostOfUsage = function(usage, element) {
		element.text(WNWaterSavingModel.waterCostFor(usage));
	}

	var refreshTotalUsage = function(usage, element) {
		element.text(Math.round(usage));
	}

	var refreshTimestamp = function(timestamp, element) {
		element.text(moment(timestamp).calendar());
	}

	var waterUsageDescriptions = [ 
	                              	[-100000, -700, "vastly less than"],
	                              	[-700, -100, "much less than"],
	                              	[-100, -50, "less than"],
	                              	[-50, -30, "less than"],
	                              	[-30, 0, "a little bit less than"],
	                              	[0, 30, "around as much as"],
	                              	[30, 50, "a little bit more than"],
	                              	[50, 100, "more than"],
	                              	[100, 700, "much more than"],
	                              	[700, 100000, "vastly more than"] ];

	var refreshUsageDescription = function(usage, average, element) {
		var description = "";
		
		var difference = usage - average;
		
		$.each(waterUsageDescriptions, function(index, descriptionRule) {
			if(descriptionRule[0] <= difference && difference < descriptionRule[1]) {
				description = descriptionRule[2];
			}
		});
		
		element.text(description);
	}


	var refreshDashboardSummary = function() {
		console.log("Start Refreshing Dashboard Summary");
		$.when(WNDatabase.getDailyUsageToNowForDays(1),
			   WNWaterSavingModel.calculateNormalProjectedUsageForCurrentMonth(),
			   WNDatabase.getAverageOverDays(14))
				.done(
						function(usages, projectedMonthlyUsage, average) {
							var currentUsage = usages[0];
							
							refreshTimestamp(currentUsage.timestamp,
										 	 $("#dashboard-current-usage-date"));
							
							refreshUsageDescription(currentUsage.usage, average,
													$("#dashboard-usage-description"));
							
							refreshTotalUsage(currentUsage.usage,
											  $('#dashboard-total-usage'));
							
							refreshTotalUsage(projectedMonthlyUsage,
							  				  $('#dashboard-projected-month-usage'));
							
							refreshCostOfUsage(projectedMonthlyUsage,
											   $('#dashboard-spent-money'));
							
							console.log("Finished Refreshing Dashboard Summary");
						});
		
	}

	var refreshDashboardChart = function() {
		console.log("Start Refreshing Dashboard Chart!");
		
		$.when(WNDatabase.getDailyUsageToNowForDays(10))
		.done(function(resultUsageSeries) {
				$.when(WNDatabase.getOverallAverageSeriesForDays(resultUsageSeries.length, 3))
				.done(
						function(averageSeries) {
							var usageSeries = convertTimeseriesToFlotFormat(resultUsageSeries);
							var series1 = {
								data : usageSeries,
								label : "Your Usage",
								color : "#2489ce",
								bars : {
									show : true,
									barWidth: 18*60*60*1000
								}
							};
							var average = {
								data : convertTimeseriesToFlotFormat(averageSeries),
								label : "Average Usage",
								color : "#b8ffc0",
								lines : {
									show : true,
									lineWidth: 1
								}
							};
							var spark = {
								data : [ usageSeries[usageSeries.length - 1] ],
								color : '#ff2020',
								bars : {
									show : true,
									barWidth: 18*60*60*1000
								}
							};
							$.plot($("#dashboard-chart-placeholder"), [ average, series1, spark ], {
								xaxis : {
									mode : "time",
									tickSize : [3, "day"]
								},
								grid : {
									borderWidth : {
										"top" : 0,
										"left" : 1,
										"bottom" : 1,
										"right" : 0
									}
								},
								bars : {
									align: "center"
								},
								legend : {
									container : "#dashboard-legend-placeholder",
									sorted : "descending",
									noColumns : 2
								}
							});
							console.log("Done Refreshing Dashboard Chart!");
						});
					});
	};
	
	var _refreshStatusTab = function() {
		$.when(WNDatabase.enoughData())
		.done(function(enoughData) {
			refreshMeterReadingBox();
			if(enoughData) {
				$("#dashboard-message-box").hide();
				$("#dashboard-chart").show();
				refreshDashboardSummary();
				refreshDashboardChart();
				refreshUsagesList();
			} else {
				$("#dashboard-chart").hide();
				refreshMoreDataMessage($("#dashboard-message-box"));
				
			}
		});
		refreshReadingsList();
	}
	
	return {
		"setUp" :  function() {
			setUpMeterReadingBox();
			setUpStandardMeterReadingBox();;
			
			_refreshStatusTab();
			
			$("#dashboard-panel1-leak-detection").click(function(event) {
				showLeakDetectionWizard();
				return false;
			});
		},
		"refresh" : function() {
			_refreshStatusTab();
		},
		"open" : function() {
			_refreshStatusTab();
		}	
	};
}();