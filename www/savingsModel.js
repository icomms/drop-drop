/*
	Drop Drop Project
	iCOMMS Group - University of Cape Town
	Initial Author: Patrick Rein (pre), patrick.rein@gmail.com
 */

var WNWaterSavingModel = (function() {
	
	var activeSavingMethods = []; 
	var weeklyModel = { "timestamp" : moment(0), "model" : []};
	
	var runModel = function(pastData) {
		$.each(activeSavingMethods, function(index, savingMethod) {
			$.each(pastData, function(index, oneUsage) {
				oneUsage.usage = oneUsage.usage - savingMethod.saving(oneUsage.usage);
			})
		});
	
		return pastData;
	};
	
	function getWeeklyModel() {
		var d = new $.Deferred();
		var lengthOfWeeklyModel = 7;
		
		if(weeklyModel.timestamp.startOf("day") == moment().startOf("day")) {
			// Cache hit
			d.resolve(weeklyModel.model);
		} else {
			// Cache miss
			$.when(WNDatabase.getDailyUsageToNowForDays(lengthOfWeeklyModel))
			.done(function(usages) {
				var result = [];
				var total = 0, key;
				
				// Group usages into days
				$.each(usages, function(index, usage) {
					key = usage.timestamp.day()
					if(!result[key]) {
						result[key] = 0;
					}
					result[key] += usage.usage;
					total += usage.usage;
				});
				var average = total / lengthOfWeeklyModel;
				// Calculate percentage of each day on total
				$.each(result, function(index, val) {
					if(val > 0) {
						result[index] = val / average;
					} else {
						result[index] = 1;
					}
					
				});
				
				weeklyModel.timestamp = moment();
				weeklyModel.model = result;
				d.resolve(result);
			});
		}
		
		return d;
	};
	
	function getNormalProjectedDailySeries(numberOfDaysToProject) {
		var d = new $.Deferred();
		
		$.when(WNDatabase.getAverageOverDays(14),
			   getWeeklyModel(),
			   WNDatabase.getTimeOfLatestReading(moment()))
	    .done(function(average, weeklyModel, latestReading) {
	    	var result = [];
	    	
	    	for(var i = 0; i < numberOfDaysToProject; i++) {
	    		var item = {"timestamp" : moment(latestReading).add("days", i), "usage" : 0 };
	    		item.usage = average * weeklyModel[item.timestamp.day()];
	    		result.push(item);
	    	}
	    	
	    	d.resolve(result);
	    });
		
		return d;
	};
	
	return {
		addSavingMethod : function(savingMethod) {
			activeSavingMethods.push(savingMethod);
		},
		removeSavingMethod : function(savingMethod) {
			activeSavingMethods = $.removeFromArray(savingMethod, activeSavingMethods);
		},
		getActiveSavingMethods : function() {
			return activeSavingMethods;
		},
		calculateProjectedSavingsForAMonth : function() {
			var d = new $.Deferred();
			var that = this;
			$.when(this.calculateNormalProjectedUsageForCurrentMonth()).done(
					function(totalUsageForMonth) {
						var normalSpendings = that.waterCostFor(totalUsageForMonth);
						
						//TODO: refine by applying weekly model to every day.
						var daysInMonth = Math.round(moment.duration(moment().endOf("month") - moment().startOf("month")).asDays());
						var projectedDailyUsage = runModel([{"usage" : totalUsageForMonth / daysInMonth}])[0].usage;
						var savedProjectionSpendings = that.waterCostFor(daysInMonth * projectedDailyUsage); //Don't look closely... 
						d.resolve(normalSpendings - savedProjectionSpendings);
					}	
			);
			return d;
		},
		calculateProjectedDailySeries : function(noOfDaysToProject) {
			var d = new $.Deferred();
			
			$.when(getNormalProjectedDailySeries(noOfDaysToProject))
			.done(function(normalSeries) {
				var result = runModel(normalSeries);
				d.resolve(result);
			});
			
			return d;
		},
		calculateNormalProjectedDailySeries : function(noOfDaysToProject) {
			return getNormalProjectedDailySeries(noOfDaysToProject);
		},
		calculateNormalProjectedUsageForCurrentMonth : function() {
			var d = new $.Deferred();
			var daysToQuery = moment().date() - 1;
			if(daysToQuery < 5) {
				// Not enough monthly data, use data from month before
				daysToQuery = 5;
			}
			 $.when(WNDatabase.getAverageOverDays(daysToQuery)).done(function(dailyAverage) {
				 var daysInMonth = Math.round(moment.duration(moment().endOf("month") - moment().startOf("month")).asDays());
				 d.resolve(daysInMonth * dailyAverage);
			});
			return d;
		},
		waterCostFor : function(amountOfWaterLitres) {
			var costTable = [ [ 6000, 0 ], [ 4500, 6.65 ], [ 9500, 12.08 ],
			      			[ 15000, 17.9 ], [ 15000, 22.12 ], [ -1, 29.16 ] ];
			var totalAmount = 0;
			for ( var costStepIndex = 0; (costStepIndex < costTable.length)
					&& (amountOfWaterLitres > 0); costStepIndex++) {
				var costStep = costTable[costStepIndex];
			    if (amountOfWaterLitres <= costStep[0] || costStep[0] < 0) {
			    	// Use up remaining usage
			    	// -1 counts as positive infinity so every remaining drop is charged
			    	// with the highest fee.
			    	totalAmount += (amountOfWaterLitres / 1000) * costStep[1];
			      	amountOfWaterLitres = 0;
			    } else {
			    	// Use the complete level
			    	totalAmount += (costStep[0] / 1000) * costStep[1];
			      	amountOfWaterLitres = amountOfWaterLitres - costStep[0];
			 	}
			 }
			 // Remaining water is charged with the highest fee.
			 return Math.round(totalAmount);
		}
	}
})();