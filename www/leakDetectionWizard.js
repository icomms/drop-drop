/*
	Drop Drop Project
	iCOMMS Group - University of Cape Town
	Initial Author: Patrick Rein (pre), patrick.rein@gmail.com
 */

function setUpLeakDetectionWizard() {
	
	$(".leak-detection-cancel").click(function(event) {
		event.stopPropagation();
		
		var confirmCallback = function(button) {
			if(button == 2) {
				// Yes pressed
				hideLeakDetectionWizard();
				resetLeakDetectionWizard();
			}
		}
		if(navigator.notification) {
			navigator.notification.confirm("Do you really want to quit the leak detection process?", confirmCallback, "Quit Leak Detection", "No,Yes");
		} else {
			hideLeakDetectionWizard();
			resetLeakDetectionWizard();
		}
	});
	
	setUpAllSteps();
	
};

/*
 * A decentral architecture of the process would be nicer but then the storing of the process status becomes harder.
 */

var LEAK_DETECTION_IMAGE1_KEY = "leakDetectionImage1";
var LEAK_DETECTION_IMAGE2_KEY = "leakDetectionImage2";
var LEAK_DETECTION_TIMER_START_KEY = "leakDetectionStartTimer";

var PROCESS_END = -1;

function resetLeakDetectionWizard() {
	applicationStorage.removeItem(LEAK_DETECTION_IMAGE1_KEY);
	applicationStorage.removeItem(LEAK_DETECTION_IMAGE2_KEY);	
	applicationStorage.removeItem(LEAK_DETECTION_TIMER_START_KEY);
	$("#leak-detection-step4-button1 > a").text("Wait...");
	$("#leak-detection-step3-button1 > a").text("Take a picture first.");
	$(".leak-detection-step").hide();
	$('#leak-meter-picture1').attr("src", "");
	$('#leak-meter-picture2').attr("src", "");
	leakWizardActive = false;
};

var leakDetectionStep1 = {
	successor : 2,
	predecessor : PROCESS_END,
	setUp : function() {
		var that = this;
		$("#leak-detection-start").click(function(event) {
			event.stopPropagation();
			showStep(that.successor);
			
		});
	}
		
}

var leakDetectionStep2 = {
	successor : 3,
	predecessor : 1,
	setUp : function() {
		var that = this;
		$("#leak-detection-step2-button1").click(function(event) {
			event.stopPropagation();
			showStep(that.successor);
		});
	}
}


var leakDetectionStep3 = {  
	successor : 4,
	predecessor : 2,
	setUp : function() {
		var that = this;
		$("#leak-detection-step3-button1").click(function(event) {
			event.stopPropagation();
			showStep(that.successor);
		});
		
		$("#leak-detection-take-picture1").click(function(event) {
			event.stopPropagation();
			
			var onSuccess = function(imagePath) {
				applicationStorage.setItem(LEAK_DETECTION_IMAGE1_KEY, imagePath);
			    var image = $('#leak-meter-picture1');
			    image.attr("src", imagePath);
			    
			    $("#leak-detection-step3-button1 > a").text("Next Step");
			    $("#leak-detection-step3-button1").click(function(event) {
					event.stopPropagation();
					showStep(that.successor);
				});
			}
	
			var onFail = function(message) {
			    alert('Failed because: ' + message);
			}
			
			navigator.camera.getPicture(onSuccess, onFail, { quality: 50, 
			    destinationType: Camera.DestinationType.FILE_URI,
		          saveToPhotoAlbum: true }); 
			
		})
	
	}
}


var leakDetectionStep4 = {
		successor : 5,
		predecessor : 3,
		start : function() {
			applicationStorage.setItem(LEAK_DETECTION_TIMER_START_KEY, moment());
			this.timer(this);
		},

		timer : function(that) {
			var timespan = _WATER_TEST_MODE ? 10 : 90*60;
			var rawDiff = timespan - moment.duration(moment().diff(moment(applicationStorage.getItem(LEAK_DETECTION_TIMER_START_KEY)))).asSeconds();
			var difference = moment.duration(rawDiff, 'seconds'); 
			$("#leak-detection-timer").text(difference.hours() + " hour " + difference.minutes() + " minutes " + Math.round(difference.seconds()) + " seconds left");
			if(difference.hours() <= 0 && difference.minutes() <= 0 && difference.seconds() <= 1) {
				// Dirty check, as this is not exact
				$("#leak-detection-step4-button1 > a").text("Next Step");
				var nextStep = that.successor;
				$("#leak-detection-step4-button1").click(function(event) {
					event.stopPropagation();
					showStep(nextStep);
				});
			} else {
				setTimeout(function() {
					that.timer(that);
				}, 1000);
			}
		},

		setUp : function() {
			var that = this;
			$("#leak-detection-step4-button1").click(function(event) {
				event.stopPropagation();
				showStep(that.successor);
			});
		}
		
}

var leakDetectionStep5 =  {
	successor : 6,
	predecessor : 4,
	setUp : function() {
		var that = this;
		$("#leak-detection-step5-button1").click(function(event) {
			event.stopPropagation();
			showStep(that.successor);
		});
		
		$("#leak-detection-take-picture2").click(function(event) {
			event.stopPropagation();
			
			var onSuccess = function(imagePath) {
				applicationStorage.setItem(LEAK_DETECTION_IMAGE2_KEY, imagePath);
			    var image = $('#leak-meter-picture2');
			    image.attr("src", imagePath);
			}

			var onFail = function(message) {
			    alert('Failed because: ' + message);
			}
			
			navigator.camera.getPicture(onSuccess, onFail, { quality: 50, 
			    destinationType: Camera.DestinationType.FILE_URI, saveToPhotoAlbum: true }); 
			
		})
	}
}

var leakDetectionStep6 = {
	successor : PROCESS_END,
	predecessor : 5,
	start : function() {
		$("#leak-meter-final-picture1").attr("src", applicationStorage.getItem(LEAK_DETECTION_IMAGE1_KEY));
		$("#leak-meter-final-picture2").attr("src", applicationStorage.getItem(LEAK_DETECTION_IMAGE2_KEY));
	},
	setUp : function() {
		$("#leak-detection-step6-button1").click(function(event) {
			event.stopPropagation();
			hideLeakDetectionWizard();
			resetLeakDetectionWizard();
		});
	}
}

var process = [leakDetectionStep1, leakDetectionStep2, leakDetectionStep3, leakDetectionStep4, leakDetectionStep5, leakDetectionStep6];
function setUpAllSteps() {
	$.each(process, function(index, step) {
		step.setUp();
	})
}

function showStep(stepId) {
	$(".leak-detection-step").hide();
	$("#leak-detection-step"+stepId).show();
	
	if(process[stepId - 1].start != undefined) {
		process[stepId - 1].start()
	}
};

function showLeakDetectionWizard() {
	leakWizardActive = true;
	$("#main-content").addClass("hidden");
	$("#leak-detection-wizard").removeClass("hidden").show();
	$("#leak-detection-step1").removeClass("hidden").show();
};

function hideLeakDetectionWizard() {
	$("#main-content").removeClass("hidden");
	$("#leak-detection-wizard").addClass("hidden");
	
	// Necessary if the screen rotation changed while the wizard was open.
	$("#main-content").foundation('section', 'reflow');
	callOpenTabHandler($("div.tabs > section.active > p.title > a"));
};