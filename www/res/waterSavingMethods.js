var WNWaterSavingMethods = [
{
	"title" : "Rainwater for your toilet",
	"text" : "Collect water when it's raining by channeling water from you roof into a barrel. You can then use that water to flush your toilet.",
	"saving" : function(usage) {
		return usage * 0.15;
	}
},
{
	"title" : "Reuse water from washing clothes",
	"text" : "Keep the water you used for washing your clothes. You can flush your toilet with it or you can also water outdoor plants with it.",
	"saving" : function(usage) {
		return usage * 0.1;
	}
},
{
	"title" : "Shower water for flushing your toilet",
	"text" : "You can also use the water you used for showering in order to flush your toilet.",
	"saving" : function(usage) {
		return usage * 0.1;
	}
},
{
	"title" : "Capture water from running tap",
	"text" : "At best you don't let any tap running without using the water. But if you have to you can at least capture the flowing water into a bucket or a bottle. You can use that water for example to water plants or wash your hands.",
	"saving" : function(usage) {
		return usage * 0.025;
	}
},
{
	"title" : "Lower the flow of your tap",
	"text" : "Get an adapter for you tap which lowers the water flow. It mixes some air into the water so you get the same volumen but use less water. The adapters are called aerators. You have to ask whether it fits your tap, as they come in different sizes.",
	"saving" : function(usage) {
		return usage * 0.05;
	}
},
{
	"title" : "Short showers",
	"text" : "Time your shower to keep it under 5 minutes.",
	"saving" : function(usage) {
		return usage * 0.05;
	}
}, {
	"title" : "Rainwater for your lawn",
	"text" : "Get a rainwater tank and water your lawn with it.",
	"saving" : function(usage) {
		return usage * 0.1;
	}
}, {
	"title" : "Early or late watering",
	"text" : "Only water your lawn in the morning or the evening instead of during the day.",
	"saving" : function(usage) {
		return usage * 0.1;
	}
},{
	"title" : "Check for leaks",
	"text" : "Check your taps and toilets for leaks and fix them. That can save a lot of money. Monitor your water bill for unusually high use. Your bill and water meter are tools that can help you discover leaks.",
	"saving" : function(usage) {
		return 0;
	}
},
{
	"title" : "Full machines",
	"text" : "Run your clothes washer and dishwasher only when they are full.",
	"saving" : function(usage) {
		return usage * 0.1;
	}
}
];

$.each(WNWaterSavingMethods, function(index, method) {
	method.id = index;
});
