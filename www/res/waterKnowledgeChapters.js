var WNKnowledgeChapters = { chapters : [
                           
{
	title : "Water: Element of Life",
	topics : [
	          {
	        	  title : "Water and Humans",
	        	  text : "<img src='img/waterhuman.gif' /><p>Water is absolutely essential for all forms of life, so also for us human beings. We experience this every day when we become thirsty. Why do we need so much water? Simply because our bodies consist of approximately 75% water. If we do not drink enough water we may dehydrate. When you dehydrate it means that you have lost more water from your muscles, than has been replaced. That is a very dangerous situation, because irreversible damage may be done to your body, and if you lose too much water you will die.</p>"
	        	  		
	          },
	          {
	        	  title : "Water on Earth",
	        	  text : "<img src='img/earth-water.jpg'"+
      	  				'<p>All forms of life on Earth have always been dependent on water for survival, and today water holds the key to survival in the future too. When Neil Armstrong landed on the moon in 1969 he described Planet Earth as "a shining blue pearl spinning in space". The blue colour is, in fact, the water that is present on Earth and the atmosphere.</p>' +
    	  				"<p>Approximately 97% of all water is found in the sea which covers about 70% of the Earth's surface. The seawater contains a large amount of salt in solution, which means that it cannot be used as it is. Only the remaining 3% is fresh water. Of this 3%, less than 1% is available for life on Earth, whilst the rest is in the form of ice at the poles, within the Earth's crust as groundwater, and in the atmosphere as water vapour. This means that very little fresh water is available on Earth in a form that can readily be used for human consumption.</p>"},
	          {
	        	  title : "South Africa is water-stressed",
	        	  text : "The amount of water on earth is constant and cannot be increased or decreased, but it is unevenly distributed across the earth. South Africa receives an annual rainfall of 492 millimetres whereas the rest of the earth receives 985 millimetres. This is nearly half the earth's average. Thus South Africa is classified as a water-stressed country."+
	        	  		 "<img src='img/annual-rain-south-africa.jpg' />" + 
	        	  		 "There is also uneven distribution of rainfall across South Africa. The eastern half of the country is much wetter than the western half due to the nature of the weather conditions. South Africa also experiences alternating periods of droughts and floods which affects the amount of water across South Africa. In addition, hot dry conditions result in a high evaporation rate. Scientists predict that with global warming, South Africa will experience much wetter wet seasons and much drier dry seasons, resulting in an increase in floods and droughts." +
	        	  		 "<img src='img/drought-flood.jpg' />" +
	        	  		 "According to the Department of Water and Environmental Affairs, the demand for water will outstrip supply in Gauteng by 2013, and in the whole of South Africa by 2025. South Africa cannot afford to build more dams and water transfer schemes as they cost large amounts of money. Thus water in South Africa is in great demand, and as the human population increases with its increasing needs for survival, the greater is the demand for water."
	          }
	          ]
},
{
	title : "Water Sources",
	topics : [
	          {
	        	  title : "Forms of Water",
	        	  text : 'Water comes in three different forms <ul class="circle"><li>As <b>vapour</b> we see it as clouds, mist and steam. <img src="img/cloud.JPG" /><img src="img/water-vapour.jpg" /></li><li>As a <b>fluid</b> we see water as rain, in streams, lakes, dams, wetlands and the sea. <img src="img/rain.jpg" /><img src="img/sea.jpg" /></li><li>As a <b>solid</b> we see water as ice in glaciers, hail, snow and frost. <img src="img/snow.jpg" /></li></ul>'
	        		  	 + '<p>All those forms of water finally provide us with drinking water.</p>'
	          },
	          {
	        	  title : "Natural Water Sources",
	        	  text : '<p>In order to undertand where the water from the tap comes from, let us first take a look at the different sources water can come from in general.</p>'
	        	  		 + "<p>The most visible source of water is so called <b>surface water</b>. It contains all the water on earth's surface. Examples are lakes, rivers and even small streams.<img src='img/lake_sa.jpg' class='img-rounded'/> <img src='img/river_sa.JPG' class='img-rounded'/>You can easily access surface water, however it is also easily polluted as everyone can put unhealthy things in it. Surface water comes from to other natural water sources: Rain and groundwater.</p>"
	        	  		 + "<p><b>Rain water</b> is also a very visible water source. Rain water fills up lakes and also sinks into the ground and becomes groundwater.</p>"
	        	  		 + '<p>Some of the rainwater seeps through the soil on to rocks below that do not let it seep any further. This is <b>underground water</b>. Sometimes due to high pressure, this water sprouts out in the form of springs at the edge of mountains. It also gathers underground in underground lakes and rivers. We can use boreholes to get access to that water. <img src="img/groundwater.jpg" /></p>'
	        	  		 + '<p>On huge water source is <b>sea water</b>. However it is too salty to be used as drinking water directly. It first has to be desalinated, which is a very extensive process, but then it is as good as any water from any of the other water sources.</p>'
	          },
	          {
	        	  title : "The water mix of South Africa",
	        	  text : 'South Africa draws water mainly from two sources. Some water is drawn directly from ground water. Most of the water however comes form surface water, mostly rivers. Dams store water coming down the river which is then distributed through the water system. <img src="img/dam_sa.jpg" /></p>'
	        		  		+ '<p>There are great differences in the water situation of different regions. Therefore water is also transported form one region to another, depending on the current amount of water each region has. So a region which had a lot of rainfall might give water to a region which suffered from a drought.</p>'
	          } ]
},
/*{
	title : "Water System",
	topics : [
	          {
	        	  title : "From the river to the tap",
	        	  text : 'The water system not only brings water to you, it also cleans the water, so it is safe to drink, and removes your wastewater. Before clean water reaches you the following things have to happen:'
	        		  		+ '<ol><li>Surface water is collected, for example the water of a river is stored in a dam. <img src="img/dam_sa.jpg" /></li>'
	        		  		+ '<li>Water from the dam is brought to a purification station which cleans the water and makes it safe to drink.</li>'
	        		  		+ '</ol>'
	          },
	          {
	        	  title : "After the drain",
	        	  text : ''
	          },
	          {
	        	  title : "Who runs the water system?",
	        	  text : ''
	          },
	          {
	        	  title : "Who works in the water sector?",
	        	  text : ''
	          }
	]

},*/
{
	title : "Water Quality",
	topics : [
	          {
	        	  title : "Drinking Water",
	        	  text : '<img src="img/drinking_water.jpg" />Drinking water is water safe enough to be consumed or used with low risk of health damages. Water from rivers or lakes isn\'t always clean and safe to drink. However the water provider purifies the water before delivering it.'
	          },
	          {
	        	  title : "Polluted Water",
	        	  text : '<img src="img/polluted_water.jpg" /><p>Sometimes water gets polluted and becomes dangerous. Pollution might be caused for example by a fault in the purification system the transportation system. Several things can pollute our water:</p>'
	        		     + '<dl>'
	        		     + '<dt>Possible Poisonous Chemicals in River Water</dt>'
	        		     + '<dd><p>Unfortunately many industries release their waste products directly into rivers or let them leak into the groundwater. These chemicals are poisonous or toxic to plants, animals and people. Some examples are:</p>'
	        		     + '<dl>'
	        		     	+ '<dt>Insecticides</dt>'
	        		     	+ '<dd>Insecticides are chemicals that are sprayed onto crops to kill the insects that eat crops. Insecticides are easily washed by the rain into streams and groundwater where they poison fish and domestic animals.</dd>'
	        		     	+ '<dt>Heavy Metals</dt>'
	        		     	+ '<dd>Heavy metals such as nickel, molybdenum, zinc, cadmium and lead are mined and processed by the mining and ore-smelting industries, many of which occur in Gauteng. These metals are easily washed into streams and groundwater. Copper and mercury are another two heavy metals, which are found in fungicides. Fungicides are also sprayed on crops and easily washed into rivers. These heavy metals are toxic to biological life including the people who may have to drink from the polluted rivers. Crops that have been irrigated with polluted water can also be dangerous. Heavy metals can build up in the body causing symptoms of poisoning.</dd>'
	        		     	+ '<dt>Problem Products</dt>'
	        		     	+ '<dd>Oil pollution is a problem, not only on sea, but also on land. Petrol and diesel is stored in underground tanks at petrol depots. When these tanks are not properly maintained, they can develop cracks allowing the petrol to leak out. Many people are also very negligent when changing the oil in their car engines. Many people just throw the oil onto the ground! This oil and petrol is washed by the rain into the groundwater. This ground water eventually surfaces at boreholes and wetlands. In the past, groundwater was considered clean since chemicals and germs were not present, but this is changing very quickly. Groundwater is becoming more and more polluted with a number of toxic substances including insecticides, petroleum products and heavy metals. South Africa is a dry country and as the human population grows, our groundwater is becoming very valuable. We must all work towards keeping this resource as clean as possible for the future when we really need it.</dd>'
	        		     	+ '<dt>Chlorine and Detergents</dt>'
	        		     	+ '<dd>Paper and pulp mills and textile factories are amongst the worst water polluters. Paper and pulp mills use up large amounts of water and produce a lot of polluted wastewater. The wastewater contains strong chemicals such as chlorine, which is used to make paper white and soft. Textile factories also release strong chemicals like caustic soda, acids, dyes and detergents into water. These strong poisons also cause bird and fish kills similar to insecticide poisoning. These chemicals are also directly poisonous to humans. Ask questions about what industries occur in your area and how they release their wastewater. Do not drink water that is downstream from a factory that releases wastewater into the river.</dd>'
	        		     	
	        		     + '</dl>'
	        		     + '</dd>'
	        		     
	        		     + '<dt> Fertilisers and Sewage</dt>'
	        		     + '<dd>'
	        		     + 'Some chemicals like fertilisers are made of substances that do occur naturally in the environment, but only in small amounts. When too much fertiliser is washed from farmlands into a river then that water will also become polluted. Human sewage or cattle excrement that is untreated also causes water pollution in the same way as fertilisers do. Human sewage also contains germs that cause diseases such as hepatitis and cholera. Soaps and washing detergents contain both natural and man-made (artificial) chemicals. Artificial chemicals like bleach and chlorine were discussed earlier. The natural chemicals can cause a pollution problem similar to that caused by fertilisers.'
	        		     + '</dd>'
	        		     + '</dl>'
	          },
	          {
	        	  title : "Water and your health",
	        	  text : 'Most diseases in the world are related to water and sanitation. To break the cycle of disease, there must be improvements in the quality of water that people use. Most rural communities in South Africa do not have access to running water, toilets or latrines and they use watercourses for defecation and urination. In many cases, where they are present, latrines are situated upstream from where the community collects their water supply. Faecal pollution of water increases the risk of infection of various diseases to those using these courses as their life supporting water source. Groundwater, which is another water source, can become contaminated through unclean irrigation water. Water related diseases could be spread in other ways, which also affect urban communities, such as insect bites and poor hygiene.'
	        		  	 + '<p>Although there are many more, these are some of the most common water related diseases in South Africa:</p>'
	        		  	 + '<dl>'
		        		  	 + '<dt>Bilharzia</dt>'
		        		  	 + '<dd>This occurs, like most diseases, in a cycle with a parasite and different hosts. Bilharzia only occurs in areas where conditions are right for the parasite to be able to complete its life cycle. The adult parasite is a worm that lives in the bladder or intestine of humans (the main host). They mate inside the body and eggs are released in the urine or faeces of the host. If an infected person defaecates and urinates into the water, the eggs hatch into the swimming form of the parasite. It then burrows into the body of a snail (the intermediate host). The snails favour slow moving water with plenty of vegetation. Here the parasite changes form and exits out into the water again. This is the stage that infects humans. If a person has contact with contaminated water the parasite will penetrate their skin and move through the body causing illness. Some symptoms of Bilharzia: an itchy rash, headaches, abdominal pain, diarrhea, bladder infections, fever, enlarged liver and swollen veins. Within South Africa, Bilharzia is most common in the Northern Province, the Lowveld and KwaZulu-Natal.</dd>'
		        		  	 
		        		  	 + '<dt>Malaria</dt>'
		        		  	 + '<dd>In South Africa Malaria is given very high priority. This disease, which also occurs as a cycle, is caused by a parasite that is transmitted by some species of female mosquitoes. The female mosquito requires a blood meal in order to obtain sufficient energy and nutrients to produce her eggs. When a mosquito bites a human, it injects saliva into the bloodstream to prevent the blood from clotting. If the mosquito is infected with the malarial parasite, the parasite will be released from the mosquito\'s saliva into the blood of the human. The parasite travels through the body and enters the red blood cells. The red blood cells eventually burst releasing the parasite into the blood stream where it is ready to be sucked up by the next mosquito. Mosquitoes breed in water, especially dams, ponds, water tanks, old car tyres, and other hollow objects that can hold water, like tins. The best way to protect yourself from Malaria is not to leave litter lying around, and to prevent getting bitten by wearing long sleeve clothing and by applying insect repellent to your skin, especially at night. Some symptoms of Malaria: fever, headache, diarrhoea, nausea, joint and muscular pains, shivering, sweating and fatigue. Malaria is distributed in the Northern Province, Mpumalanga, Northern KwaZulu-Natal and parts of the northern Cape.</dd>'
		        		  	 
		        		  	 + '<dt>Cholera</dt>'
		        		  	 + '<dd>Cholera is a disease that is caused by bacteria (Vibrio cholerae) that is spread through water contaminated by faeces from an infected person. The bacteria produce a toxin that causes the small intestine to secrete large amounts of fluid, which leads to fluid loss, i.e. diarrhoea and vomiting. People who do not wash their hands after using the toilet can spread the disease. It can also be spread when human faeces are used as a fertiliser for vegetable crops. It is important to remember that even if a person does not show symptoms of the disease, they could still be infected and spread the disease. Cholera can be found in most places where there is poor sanitation. Some symptoms of Cholera: diarrhoea and vomiting. People who have the disease should drink plenty of clean water to prevent dehydration.</dd>'
	        		  	 + '</dl>'
	          },
	          {
	        	  title : "Making Water clean",
	        	  text : 'If you don\'t have a water purification station to clean your river water what can be done to clean the water? There are 5 ways to make your water safe to drink'
	        		  	+ '<dl>'
	        		  	+ '<dt>Boiling</dt>'
	        		  	+ '<dd>Boiling water kills any germs that might be in the water.'
	        		    + '<ol><li>Boil water in a pot.</li> <li>Allow to cool.</li> <li>Keep this boiled water covered with a lid or clean cloth to protect it from being contaminated by flies and dirt.</li></ol>'
	        		    + '</dd>'
	        		    + '<dt>Adding Bleach</dt>'
	        		  	+ '<dd>Bleach is strong smelling and contains chlorine which kills harmful germs in the water.'
	        		    + '<ol><li>Buy a bottle of bleach from your local shop.</li> <li>Add one teaspoon of bleach to 20 litres of water.</li> <li>Allow to stand overnight for a minimum of 2 hours.</li> <li>Keep the water covered with a cloth or lid to keep out flies and dirt. Your water is now safe to drink.</li></ol>'
	        		    + '</dd>'
	        		    + '<dt>Adding Iodine</dt>'
	        		  	+ '<dd>Iodine will also kill germs in the water. Please do not use iodine if you are allergic to it.'
	        		    + '<ol><li>Add 5 drops (1 drop = 0.05ml) iodine to one litre of water.</li> <li>Cover the water with a cloth or lid and let it stand for 15 hours.</li> <li>If the water is cloudy, filter it through a clean cloth and double the dose to 10 drops of iodine per litre of water.</li></ol>'
	        		    + '</dd>'
	        		    + '<dt>Adding Water Purification Tablets</dt>'
	        		  	+ '<dd>There are a number of water purification tablets available. These can be purchased from a chemist or camping shop. Please follow the instructions on the pack carefully. When the dirt settles to the bottom of the container then filter the clean water through a clean cloth. The dirt will be left on the cloth. Be sure to keep the clean water covered with a cloth or lid.'
	        		    + '</dd>'
	        		    + '<dt>Solar Disinfection</dt>'
	        		  	+ '<dd>Fill a clear plastic bottle with water and leave in the hot sun for two hours. If half the bottle is painted black and it is placed on a corrugated iron sheet, the heating process is speeded up. The combination of warm water and ultraviolet radiation from the sun kills most micro-organisms. This is particularly effective for killing the cholera bacterium.'
	        		    + '</dd>'
	        		    + '</dl>'
	          },
	          {
	        	  title : "Watching our water",
	        	  text : '<p>Several organizations watch the quality of our water. The "Rand Water" water board for example publishes information about the quality of the drinking water online.</p>'
	        		  		+ '<p>The government also introduced the Blue Drop Initiative which watches the water quality all over the country. You can find up to date information about the water quality in your region on the <a href="http://www.dwaf.gov.za/dir_ws/DWQR/Default.asp?Pageid=1">My Water</a> website.</p>'
	          },
	          {
	        	  title : "Watch your water!",
	        	  text : '<h4>Important Things That You Can Do to Decrease the Risk of Disease</h4>'
	        	  		 + '<ul>'
	          					+ '<li>Do not defaecate or urinate near water sources;</li>'
				        	  	 + '<li>Wash your hands with soap and water after going to the toilet;</li>'
				        	  	  + '<li>Do not drink water that you think might be unclean - boil it if you are unsure;</li>'
				        	  	  + '<li>Wash all fruit and vegetables well before eating them &amp; do not cook with unclean water;</li>'
				        	  	  + '<li>Do not leave empty containers or any litter lying around for disease transmitting insects to breed in;</li>'
				        	  	  + '<li>If you have access to pit latrine facilities, ensure that they are away from sources that are used for drinking and bathing. The pit should not penetrate groundwater.</li>'
	        	  	     + '</ul>'
	        		  	 + '<p>Insecticides are easily washed by the rain into streams and groundwater where they poison fish and domestic animals. Many insecticides are stored for a long time in the bodies of animals and can end up in the meat, fish, egg and milk that you eat. Fruits and vegetables that have been sprayed with insecticides also remain poisonous for many days afterwards and must be washed very well before eating.</p>'
	        		  	 + '<p>Ask questions about the use of insecticides in your area. Keep a look out for fish or birds that have been poisoned. Dead fish and birds provide a good indicator of chemical pollution. If you suspect pollution by insecticides then contact your local council as soon as possible.</p>'
	        		  	 + '<p>In the past, toxic waste products were dumped into the rivers or into landfill sites close to where people lived causing health problems and even death. Today all South Africans have a constitutional right to a clean and safe environment. Make sure that you remain informed and observant so that you can prevent toxic chemicals from being used in your environment. If you suspect water pollution in your area then contact your local council.</p>'
	        		  	 + '<p>Remember that the detergents you use at home are just as poisonous so use only biodegradable products. Read the label on the product that you are buying; this will tell you what the product is made of and if it is environmentally friendly.</p>'
	        		  	
	          } ]

},
{
	title : "Imprint",
	topics : [
	         {
	        	 title : "Sources",
	        	 text : "<p>The articles are based on the articles you can find on <a href='http://www.waterwise.co.za/site/home.html'>Waterwise</a>, which is an information website by Rand Water. Thanks to Rand Water for the liberal allowance of using their material.</p>"
	        		 	+ '<li>The picture of a cloud in the "Forms of Water" Section was taken by: Santhosh kumar.</li>'
	        		 	+ '<li>The picture of snow in the "Forms of Water" Section was taken by: Henryk Zychowski</li>' 
	        		 	+ '<li>The picture of rain in the "Forms of Water" Section was taken by: Edal Anton Lefterov</li>' 
	        		 	+ '<li>Attributes for the picture of table mountain in the "Forms of Water": www.namastesouthafrica.com</li>'
	        		 	+ '<li>Parts of the descriptions about ground water are from the article "What are the different sources of water ?" written by VAIBHAV PARAKH</li>'
	        		 	+ '<li>The list of water purification methods is from "Exploring Water", Umgeni Water, 1994; "Elan", Medihelp, 2001; New Scientist Magazine, 2000"</li>'
	        		 	+ '<li>Picture of clean drinking water taken by: Alexander Anlicker</li>'
	        		 	+ '<li>Illustration of water contained in a human body: http://www.dorchesterhealth.org/water.htm</li>'
	         },
	         {
	        	 title : "iCOMMS - About us",
	        	 text : '<img src="img/icommsLogo.jpg" /><p>The UCT iCOMMS team is&nbsp; dedicated to developing context-relevant tools that will help governments and decision-makers to collect the information they require to deliver better services and to monitor policy implementations.</p>'
	        		 	+ "<p>The team aims to identify and help develop low-cost and sustainable tools appropriate to communities, and putting this technology to use in:</p>"
	        		 	+ '<ul>'
	        		 	+ '<li>Improving water quality, health care and conservation</li>'
	        		 	+ '<li>Supporting communities in collecting appropriate water quality, health-care and conservation information to aid government agency decision-making</li>'
	        		 	+ '<li>Promoting the use of open source software for the development of spatial data</li>'
	        		 	+ '<li>Applications for mobile phones</li>'
	        		 	+ '<li>Attracting funding for and promoting study in the fields above.</li>'
	        		 	+ '</ul>'
	         }
	         ]

}] };

$.each(WNKnowledgeChapters.chapters, function(index, chapter) {
	chapter.id = index;
	$.each(chapter.topics, function(index, topic) {
		topic.id = index;
	});
});