/*
	Drop Drop Project
	iCOMMS Group - University of Cape Town
	Initial Author: Patrick Rein (pre), patrick.rein@gmail.com
 */

var WNTabReporting = function() {
	
	var setUpReportingForm = function() {
		$("#save-report-call-button").click(function(event) {
			event.stopPropagation();
		
			var messageBox = $("#report-call-form-message");
			messageBox.hide();
			messageBox.removeClass("alert");
			messageBox.text("");
			
			var nameField = $("#report-call-form #name");
			var numberField = $("#report-call-form #phone-number");
			var organizationField = $("#report-call-form #orga");
			var reasonField = $("#report-call-form #reason");
			var outcomeField = $("#report-call-form #outcome");
			
			$.when(WNDatabase.storeCall(organizationField.val(), nameField.val(), numberField.val(), reasonField.val(), outcomeField.val()))
			.done(function(success) {
				if(success) {
					// Clear form and show success message
					nameField.val("");
					numberField.val("");
					organizationField.val("");
					reasonField.val("");
					outcomeField.val("");
					
					messageBox.text("Call successfully saved.");
					messageBox.addClass("success");
					messageBox.show();
					setTimeout(function() {
						messageBox.removeClass("success");
						messageBox.hide();
						messageBox.text("");
					}, 5000);
					refreshReportedCalls();
				} else {
					// Show error message
					messageBox.text("There was an error saving the call. Restart the application and try again.");
					messageBox.addClass("alert");
					messageBox.show();
				}
			})
			
		});
	}
	
	var refreshReportedCalls = function() {
		Handlebars.registerHelper('human-time', function() {
			  return new Handlebars.SafeString(
			    this.timestamp.format("Do MMM YY, hh:mm A")
			  );
		});
		
		$("#calls-list").children().remove();
		
		$.when(WNDatabase.getCalls())
		.done(function(calls) {
			var source   = $("#water-calls-template").html();
			var template = Handlebars.compile(source);
			var html = template({"calls" : calls});
			
			$("#calls-list").append(html);
		});	
	}
	
	return {
		"setUp" :  function() {
			setUpReportingForm();
			
			refreshReportedCalls();
			
			$("#report-call-form-message").hide();
		},
		"refresh" : function() { },
		"open" : function() { }	
	};
}();