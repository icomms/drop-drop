/*
	Drop Drop Project
	iCOMMS Group - University of Cape Town
	Initial Author: Patrick Rein (pre), patrick.rein@gmail.com
 */

var StarTrack = function() {

	var trackings = [];
	var callbacks = [];
	
	var codeMap = {};
	
	function initializeTracking(trackSpots) {
		$.each(trackSpots, function(index, trackSpot) {
			codeMap[index.toString()] = trackSpot.name;
			var element = trackSpot.element();
			if(element) {
				if(!trackSpot.trackData) {
					element.on(trackSpot.event, function(e) {
						track(index);
					});
				} else {
					element.on(trackSpot.event, function(e) {
						track(index, trackSpot.trackData(this));
					});
				}
			} else {
				console.warn("[starTrack] Could not find element of tracking: " + trackSpot.name);
			}
		});
	};

	function track(code, data) {
		trackings.push([moment().valueOf(), code, data]);
		var noOfTrackings = trackings.length;
		$.each(callbacks, function(index, callback) {
			if(noOfTrackings % callback[0] == 0) {
				callback[1]();
			}
		})
	};
	
	return {
		initialize : function(trackSpots) {
			
			var additionalSpots = $.map($(".track"), function(element) {
				var element = $(element);
				var tempSpot = { "element" : function() { return element },
						  "name" : element.attr("data-track-name"),
						  "event" : element.attr("data-track-event") != undefined ? element.attr("data-track-event") : "click" };
				
				return tempSpot;
			});
			
			initializeTracking(trackSpots.concat(additionalSpots));
		},
		flushToString : function() {
			var result = "";
			$.each(trackings, function(index, tracking) {
				result += tracking[0] + "," + tracking[1];
				if(tracking[2]) {
					result += "," + tracking[2];
				}
				result += "\n";
			})
			trackings = [];
			return result;
		},
		metaInformation : function() {
			var result = "";
			$.each(Object.keys(codeMap), function(index, key) {
				result += key + "," + codeMap[key] + "\n";
			})
			return result;
		},
		onEachNTrackings : function(numberOfTrackings, callback) {
			callbacks.push([numberOfTrackings, callback]);
		},
		registerEvent : function(code, description) {
			if(code in codeMap) {
				return false;
			} else {
				codeMap[code] = description;
				return true; 
			}
		},
		track : function(code, data) {
			if(code in codeMap) {
				track(code, data);
			} else {
				console.warn("[StarTrack] Can't track " + code + " as it wasn't registered as a code yet.")
			}
		}
	}

}();

var wnTrackSpots = [
                    {
                    	"name" : "status-tab",
                    	"element" : function () { return $("#main-content > section:nth-child(1) > p > a") },
                    	"event" : "click"
                    },
                    {
                    	"name" : "saving-tab",
                    	"element" : function () { return $("#main-content > section:nth-child(2) > p > a") },
                    	"event" : "click"
                    },
                    {
                    	"name" : "report-tab",
                    	"element" : function() { return $("#main-content > section:nth-child(3) > p > a") },
                    	"event" : "click"
                    },
                    {
                    	"name" : "knowledge-tab",
                    	"element" : function() { return $("#main-content > section:nth-child(4) > p > a") },
                    	"event" : "click"
                    },
                    {
                    	"name" : "reading-box-save",
                    	"element" : function() { return $("#meter-reading-box #save-reading-button")  },
                    	"event" : "click",
                    	"trackData" : function(element) { return $("#meter-reading-box > div > div.columns:first-child > input").val() }
                    },
                    {
                    	"name" : "reading-box-skip",
                    	"element" : function() { return $("#meter-reading-box #meter-reading-box-action1") },
                    	"event" : "click"
                    },
                    {
                    	"name" : "reading-box-meter-explanation",
                    	"element" : function() { return $("#meter-reading-box #meter-reading-box-action2")  },
                    	"event" : "click"
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""

                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    },
                    {
                    	"name" : "",
                    	"element" : function() {  },
                    	"event" : ""
                    }


                    ];

function wnExtractMethodId() {
	
}